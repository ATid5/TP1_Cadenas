////#############################################################################
// ARCHIVO : launcher.h
// AUTORES : Alejandro Tidele - Cristian Castillo
// FECHA DE CREACION : 06/04/2018.
// ULTIMA ACTUALIZACION: 20/05/2018.
// LICENCIA : GPL (General Public License) - Version 3.
//=============================================================================
// SISTEMA OPERATIVO : Windows 10.
// IDE : Code::Blocks - 17.12
// COMPILADOR : GNU GCC Compiler (Linux) / MinGW (Windows).
// LICENCIA : GPL (General Public License) - Version 3.
//=============================================================================
// DESCRIPCION:
// Librer�a intermedia que se encarga de ejecutar las funciones de "cadenas.h".
/////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "../CSYSTEM/csystem.h" // Libreria multiplataforma.
#include "../CADENAS/cadenas.h" // Libreria que contiene la informacion de las funciones String.

using namespace std;

namespace launch
{
//==============================================================================
// DECLARACION DE VARIABLES GLOBALES A UTILIZAR EN TODO EL SISTEMA
//------------------------------------------------------------------------------
char option[200], entry[200];
bool isValidEntry = false;
int colour = 6;
//==============================================================================
// DECLARACION DE PROTOTIPOS A UTILIZAR EN EL SISTEMA
//------------------------------------------------------------------------------
void showDocumentation(unsigned short int numOfDoc, const char functionName[200],
                       const char action[200], const char parameters[200],
                       const char returns[200], const char notes[200] = NULL);
void getValidatedEntry(bool needChar = false);
void setOption();
void showPrettyMenu();
bool isValidOption();
//*****************************************************************************
// DEFINICION DE LAS FUNCIONES DE PRESENTACION
//=============================================================================
void strLen()
{
    showDocumentation(1,"strLen",
                      "Cuenta la cantidad de caracteres que\t *\n\t*\t\tintegran una cadena.\t\t\t *",
                      "cadena de caracteres.\t\t *",
                      "un entero que representa la cantidad de *\n\t*\t\tcaracteres con excepcion del barra cero. *");

    getValidatedEntry();
    if (isValidEntry)
        cout << "\tLa cadena mide " << str::strLen(entry) << " caracteres.";
}

void strCpy()
{
    showDocumentation(2,"strCpy","Copia una cadena en otra.\t\t *","Cadena destino, cadena fuente.\t *","Nada.\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        cout << "\tCadena origen: " << entry << endl;
        cout << "\tCadena copiada: " << firstEntry << endl;
    }
}

void strCat()
{
    showDocumentation(3,"strCat","Concatena dos cadenas dejando el resultado*\n\t*\t\ten la cadena inicial.\t\t\t *","Cadena inicial, cadena final.\t *","Nada.\t\t\t\t\t *","La cadena inicial debe ser tan larga como *\n\t*\t\tla suma.\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry();
        if (isValidEntry)
        {
            str::strCat(firstEntry, entry);
            cout << "\tLa concatenacion de los ingresos es: " << firstEntry;
        }
    }
}

void strCmp()
{
    showDocumentation(4,"strCmp","Compara dos cadenas.\t\t\t *","Cadena 1, cadena 2.\t\t\t *","- Un 0 si son iguales.\t\t\t *\n\t*\t\t - Un numero positivo si la primera es \t *\n\t*\t\t   mayor que la segunda.\t\t *\n\t*\t\t - Un numero negativo si la segunda es\t *\n\t*\t\t   mayor que la primera.\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry();
        if (isValidEntry)
        {
            int compare = str::strCmp(firstEntry, entry);
            cout << "\tLa cadena 1 es " << (compare == 0 ? "igual" : compare == 1 ? "mayor" : "menor") << " a la cadena 2." ;
        }
    }
}

void strFind()
{
    showDocumentation(5,"strFind","Busca un caracter dentro de una cadena.\t *","Cadena, caracter.\t\t\t *","- La posicion donde se produce la\t *\n\t*\t\t   primera ocurrencia.\t\t\t *\n\t*\t\t - Un -1 si no lo encuentra.\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry(true);
        if (isValidEntry)
        {
            int position = str::strFind(firstEntry, entry);
            if (position >= 0)
                cout << "\tEl caracter \"" << entry <<  "\" se encuentra en la posicion \"" << position << "\".";
            else
                cout << "\tNo se encontro el caracter solicitado." ;
        }
    }
}

void strCnt()
{
    showDocumentation(6,"strCnt","Cuenta cuantas de veces que aparece un\t *\n\t*\t\t caracter dentro de una cadena.\t\t *","Cadena, caracter.\t\t\t *","La cantidad de veces que aparece el\t *\n\t*\t\t caracter en la cadena.\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry(true);
        if (isValidEntry)
        {
            int times = str::strCnt(firstEntry, entry);
            if (times)
                cout << "\tLa letra \"" << entry[0] << "\" se repite " << times << (times > 1 ? " veces." : " vez.");
            else
                cout << "\tNo se ha encontrado la letra \"" << entry[0] << "\".";
        }
    }
}

void strInv()
{
    showDocumentation(7,"strInv","Invierte los caracteres de una cadena.\t *","Cadena.\t\t\t\t *","Nada.\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        str::strInv(entry);
        cout << "\tLa cadena invertida es: " << entry;
    }
}

void strRpl()
{
    showDocumentation(8,"strRpl","Reemplaza el caracter de una posicion dada*\n\t*\t\t de una cadena por otro.\t\t *","Cadena, nuevo caracter, posicion.\t *","Nada.\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry(true);
        if (isValidEntry)
        {
            char position[200];
            cout << "\tIngrese la posicion que desea modificar: ";
            cin >> position;
            cin.ignore();

            int pos = str::strToInt(position);

            if (pos >= 0)
            {
                if (pos < str::strLen(firstEntry))
                {
                    str::strRpl(firstEntry, entry, pos);
                    cout << "\tLa cadena resultante es: " << firstEntry;
                }
                else
                {
                    cout << "\tNo existe la posicion \"" << pos << "\" en la cadena ingresada.";
                }
            }
            else
            {
                cout << "\tNo ha ingresado una posicion valida.";
            }
        }
    }
}

void strTrunc()
{
    showDocumentation(9,"strTrunc","Trunca una cadena en una posicion dada.\t *","Cadena, posicion.\t\t\t *","Nada.\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char position[200];
        cout << "\tIngrese la posicion que desea cortar: ";
        cin >> position;
        cin.ignore();

        int pos = str::strToInt(position);

        if (pos >= 0)
        {
            if (pos < str::strLen(entry))
            {
                str::strTrunc(entry, pos);
                cout << "\tLa cadena resultante es: " << entry;
            }
            else
            {
                cout << "\tNo existe la posicion \"" << pos << "\" en la cadena ingresada.";
            }
        }
        else
        {
            cout << "\tNo ha ingresado una posicion valida.";
        }
    }
}

void strSub()
{
    showDocumentation(10,"strSub","Busca una subcadena dentro de una cadena.*","Cadena, subcadena.\t\t\t *","- La posicion  inicial de la subcadena.*\n\t*\t\t  - Un -1 si no se encuentra.\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        char firstEntry[200];
        str::strCpy(firstEntry, entry);
        getValidatedEntry();
        if (isValidEntry)
        {
            int position = str::strSub(firstEntry, entry);
            if (position >= 0)
                cout << "\tLa subcadena \"" << entry << "\" comienza en la posicion \"" << position << "\".";
            else
                cout << "\tNo se ha encontrado la subcadena." ;
        }
    }
}

void strToUpper()
{
    showDocumentation(11,"strToUpper","Convierte una cadena a mayusculas.\t\t *","Cadena.\t\t\t\t\t *","Nada.\t\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        str::strToUpper(entry);
        cout << "\tLa cadena en mayusculas es: " << entry;
    }
}

void strToLower()
{
    showDocumentation(12,"strToLower","Convierte una cadena a minusculas.\t\t *","Cadena.\t\t\t\t\t *","Nada.\t\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        str::strToLower(entry);
        cout << "\tLa cadena en minusculas es: " << entry;
    }
}

void strToInt()
{
    showDocumentation(13,"strToInt","Convierte una cadena de caracteres a un\t\t *\n\t*\t\tnumero entero.\t\t\t\t\t *","Cadena.\t\t\t\t\t *","El numero entero.\t\t\t\t *","Los elementos de la cadena deben ser\t\t *\n\t*\t\tcaracteres cuyo codigo ASCII se encuentre\t *\n\t*\ten el rango 48..57\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        int number = str::strToInt(entry);
        if (number > 0)
            cout << "\tEl numero entero es: " << number;
        else
            cout << "\n\tEl valor ingresado no se encuentra en el rango ASCII\n\tde 48 a 57 (0 a 9).";
    }
}

void intToStr()
{
    showDocumentation(14,"intToStr","Convierte un numero entero a cadena.\t\t *","Numero, cadena.\t\t\t\t *","Nada.\t\t\t\t\t\t *");

    getValidatedEntry();
    if (isValidEntry)
    {
        int number = str::strToInt(entry);
        if (number > 0)
        {
            char numberToString[200];
            str::intToStr(number, numberToString);
            cout << "\tEl numero convertido a cadena es: " << numberToString;
        }
        else
            cout << "\n\tEl valor ingresado no es un numero.";
    }
}

//=============================================================================
// FUNCION : void showDocumentation(numOfDoc, functionName, action, parameters,
//                                  returns, notes)
// ACCION : Muestra gen�ricamente la documentacion de cada funcion, personali -
//          zadas por los par�metros enviados.
// PARAMETROS: - int numOfDoc: N�mero de la documentacion segun la funcion
//                             que se haya elegido mostrar.
//
//             - char* functionName: Nombre de la funcion elegida para mostrar.
//             - char* action: Accion de la funcion elegida para mostrar.
//             - char* parameters: Parametros que necesita la funcion elegida
//                                 para mostrar.
//             - char* returns: Lo que devuelve la funcion elegida para mostrar.
//             - char* notes: Notas a tener en cuenta de la funcion si las
//                             hubiera.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void showDocumentation(unsigned short int numOfDoc, const char functionName[200], const char action[200], const char parameters[200], const char returns[200],const char notes[200])
{
    //Documentaci�n gen�rica.
    cout << "\t*---------------------FUNCION " << functionName << "---------------------*" << endl;
    cout << "\t*3." << numOfDoc << "   " << functionName << "\t\t\t\t\t\t *"    << endl;
    cout << "\t*3." << numOfDoc << ".1 Accion: "     << action << endl;
    cout << "\t*3." << numOfDoc << ".2 Parametros: " << parameters << endl;
    cout << "\t*3." << numOfDoc << ".3 Devuelve: " << returns << endl;
    if (notes != NULL)
    {
        cout << "\t*\tNotas: " << notes << endl;
    }
    cout << "\t*--------------------------------------------------------*" << endl;
}

//=============================================================================
// FUNCION : void getValidatedEntry()
// ACCION : Valida que la entrada de datos no sea mayor a 100 caracteres y
//          guarda por referencia la entrada y un estado de validaci�n.
// PARAMETROS: - int numOfDoc: N�mero de la documentacion segun la funcion
//                             que se haya elegido mostrar.
//
//             - char* functionName: Nombre de la funcion elegida para mostrar.
//             - char* action: Accion de la funcion elegida para mostrar.
//             - char* parameters: Parametros que necesita la funcion elegida
//                                 para mostrar.
//             - char* returns: Lo que devuelve la funcion elegida para mostrar.
//             - char* notes: Notas a tener en cuenta de la funcion si las hubiera.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void getValidatedEntry(bool needChar)
{
    cout << "\t" << (needChar ? "Ingrese una letra: " : "Ingrese una cadena menor a 99 caracteres: ");
    sys::getline(entry, 1024);

    isValidEntry = (needChar ? str::strLen(entry) == 1 : str::strLen(entry) <= 100);

    if (! isValidEntry)
        cout << "\t" << (needChar ? "No se ha ingresado un caracter valido." : "La longitud de la cadena es mayor a 100 caracteres.");
}

//=============================================================================
// FUNCION : void showOptions()
// ACCION : Muestra las opciones que podr� utilizar el usuario.
// PARAMETROS: Ninguno.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void showOptions()
{
    sys::cls(); //Limpio la consola.

    launch::showPrettyMenu(); //Muestro cartel de menu.

    const char *_PRE = "\t\t*  \t\t ";
    const char *_POST = "  *";
    cout << "\n\t\t* * * * * * * * * * * * * * * * * * * * * *" << endl;
    cout << _PRE << "A - strLen\t\t"         << _POST << endl;
    cout << _PRE << "B - strCpy\t\t"         << _POST << endl;
    cout << _PRE << "C - strCat\t\t"         << _POST << endl;
    cout << _PRE << "D - strCmp\t\t"         << _POST << endl;
    cout << _PRE << "E - strFind\t\t"        << _POST << endl;
    cout << _PRE << "F - strCnt\t\t"         << _POST << endl;
    cout << _PRE << "G - strInv\t\t"         << _POST << endl;
    cout << _PRE << "H - strRpl\t\t"         << _POST << endl;
    cout << _PRE << "I - strTrunc\t\t"       << _POST << endl;
    cout << _PRE << "J - strSub\t\t"         << _POST << endl;
    cout << _PRE << "K - strToUpper\t\t"     << _POST << endl;
    cout << _PRE << "L - strToLower\t\t"     << _POST << endl;
    cout << _PRE << "M - strToInt\t\t"       << _POST << endl;
    cout << _PRE << "N - intToStr\t\t"       << _POST << endl;
    cout << _PRE << "S - SALIR\t\t"          << _POST << endl;
    cout << _PRE << "X - CAMBIAR EL COLOR\t" << _POST << endl;
    cout << "\t\t* * * * * * * * * * * * * * * * * * * * * *" << endl;

    setOption(); //Establece la accion que desea tomar el usuario.
}

//=============================================================================
// FUNCION : void setOption()
// ACCION : Establece la opcion que eligio el usuario.
// PARAMETROS: Ninguno.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void setOption()
{
    cout << endl;
    cout << "\t\tIngrese una opcion: ";
    sys::getline(option, 1024);

    if (! isValidOption())
    {
        cout << endl << "\t\tHa ingresado una opcion incorrecta..";
        cin.get();
        showOptions();
    }
}

//=============================================================================
// FUNCION : void takeDecition()
// ACCION : Toma la decisi�n que el usuario eligi�.
// PARAMETROS: Ninguno.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void takeDecition()
{
    //Limpio la consola
    sys::cls();

    if (option[0] == 'a' || option[0] == 'A')
        strLen();
    else if (option[0] == 'b' || option[0] == 'B')
        strCpy();
    else if (option[0] == 'c' || option[0] == 'C')
        strCat();
    else if (option[0] == 'd' || option[0] == 'D')
        strCmp();
    else if (option[0] == 'e' || option[0] == 'E')
        strFind();
    else if (option[0] == 'f' || option[0] == 'F')
        strCnt();
    else if (option[0] == 'g' || option[0] == 'G')
        strInv();
    else if (option[0] == 'h' || option[0] == 'H')
        strRpl();
    else if (option[0] == 'i' || option[0] == 'I')
        strTrunc();
    else if (option[0] == 'j' || option[0] == 'J')
        strSub();
    else if (option[0] == 'k' || option[0] == 'K')
        strToUpper();
    else if (option[0] == 'l' || option[0] == 'L')
        strToLower();
    else if (option[0] == 'm' || option[0] == 'M')
        strToInt();
    else if (option[0] == 'n' || option[0] == 'N')
        intToStr();
    else if (option[0] == 's' || option[0] == 'S')
    {
        exit(0);
    }

    if (option[0] == 'x' || option[0] == 'X')
    {
        colour = sys::random(7) + 1; //Fuera color negro
    }
    else
    {
        cout << "\n\n\tPresione ENTER para volver al Menu de Opciones." << endl;
        cin.get();
    }
}

//=============================================================================
// FUNCION : bool isValidOption()
// ACCION : Valida si los caracteres ingresados son correctos.
// PARAMETROS: Ninguno.
// DEVUELVE : Booleano.
//-----------------------------------------------------------------------------
bool isValidOption()
{
    int length = str::strLen(option);
    if (length != 1)
        return false;
    int o = (int) option[0];
    if ((o >= 97 && o <= 110) || (o >= 65 && o <= 78) || o == 115 || o == 83 || o == 120 || o == 88)
        return true;

    return false;
}

//=============================================================================
// FUNCION : void showPrettyMenu()
// ACCION : Muestra un cartel de "Menu" colorido.
// PARAMETROS: Ninguno.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void showPrettyMenu()
{
    cout << "\033[1;31m\t\t  __    __   _____   __     __   __     __ " << endl;
    cout << "\033[1;31m\t\t |///  ///| |/////| |///   |//| |//|   |//|" << endl;
    cout << "\033[1;33m\t\t |////////| |/|___  |///// |//| |//|   |//|" << endl;
    cout << "\033[1;34m\t\t |/|////|/| |/////| |//////|//| |//|___|//|" << endl;
    cout << "\033[1;32m\t\t |/| // |/| |/|___  |//|//////| |/////////|" << endl;
    cout << "\033[1;35m\t\t |/|    |/| |/////| |//|  ////| |/////////|" << endl;
    cout << "\033[1;3" << colour << "m";
}
}
//=============================================================================
//                            FIN DE ARCHIVO
//#############################################################################
