////#############################################################################
// ARCHIVO : cadenas.h
// AUTORES : Alejandro Tidele - Cristian Castillo
// FECHA DE CREACION : 06/04/2018.
// ULTIMA ACTUALIZACION: 20/05/2018.
// LICENCIA : GPL (General Public License) - Version 3.
//=============================================================================
// SISTEMA OPERATIVO : Windows 10.
// IDE : Code::Blocks - 17.12
// COMPILADOR : GNU GCC Compiler (Linux) / MinGW (Windows).
// LICENCIA : GPL (General Public License) - Version 3.
//=============================================================================
// DESCRIPCION:
// Librer�a donde se definen y se desarrollan las funciones solicitadas.
/////////////////////////////////////////////////////////////////////////////////

namespace str
{
//*****************************************************************************
// DEFINICION DE LAS FUNCIONES
//=============================================================================
// FUNCION : int strLen(char* strToCount)
// ACCION : Cuenta la cantidad de caracteres que integran una cadena.
// PARAMETROS: char* stringToCount -> Lista de caracteres que se van a contar.
// DEVUELVE : int --> Tama�o de la lista de caracteres.
//-----------------------------------------------------------------------------
int strLen(char* strToCount)
{
    int i;
    for (i = 0; strToCount[i] != '\0'; i++);
    return i;
}

//-----------------------------------------------------------------------------
// FUNCION : void strCpy(char* &strCopy, char* strOrigin)
// ACCION : Copia una cadena en otra.
// PARAMETROS: char* &strCopy -> Lista de caracteres donde se copiar�n los datos.
//             char* strOrigin -> Lista de origen de los datos.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strCpy(char (&strCopy)[200], char strOrigin[200])
{
    for (int i = 0; i <= 100; i++)//Inicializo la entrada
        strCopy[i] = '\0';

    for (int i = 0; strOrigin[i] != '\0'; i++)
        strCopy[i] = strOrigin[i];
}

//-----------------------------------------------------------------------------
// FUNCION : void strCat(char* &strOne, char* strTwo)
// ACCION : Concatena dos cadenas dejando el resultado en la cadena inicial.
// PARAMETROS: char* &strOne -> Lista de caracteres iniciales donde se devolver�
//                              el resultado de la concatenaci�n.
//             char* strTwo -> Lista de caracteres secunadarios a concatenar.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strCat(char (&strOne)[200], char strTwo[200])
{
    int initialSize = strLen(strOne);

    for (int i = 0; strTwo[i] != '\0'; i++)
        strOne[initialSize + i] = strTwo[i];
}

//-----------------------------------------------------------------------------
// FUNCION : int strCmp(char* strOne, char* strTwo)
// ACCION : Compara dos cadenas.
// PARAMETROS: char* strOne -> Lista de caracteres inicial.
//             char* strTwo -> Lista de caracteres secundaria.
// DEVUELVE : Un 0 si son iguales.
//            Un n�mero positivo si la primera es mayor que la segunda.
//            Un n�mero negativo si la segunda es mayor que la primera.
//-----------------------------------------------------------------------------
int strCmp(char* strOne, char* strTwo)
{
    int contA = 0, contB = 0;

    for (int i = 0; strOne[i] != '\0'; i++)
        contA += (int) strOne[i];

    for (int i = 0; strTwo[i] != '\0'; i++)
        contB += (int) strTwo[i];

    return contA == contB ? 0 : contA > contB ? 1 : -1;
}

//-----------------------------------------------------------------------------
// FUNCION : int strFind(char* haystack, char needle[1])
// ACCION : Busca un car�cter dentro de una cadena.
// PARAMETROS: char* haystack -> Lista de caracteres donde se va a buscar.
//             char* needle -> Caracter que se debe hallar.
// DEVUELVE : Un 0 si son iguales.
//            Un n�mero positivo si la primera es mayor que la segunda.
//            Un n�mero negativo si la segunda es mayor que la primera.
//-----------------------------------------------------------------------------
int strFind(char* haystack, char needle[1])
{
    for (int i = 0; haystack[i] != '\0'; i++)
        if (haystack[i] == needle[0])
            return i;

    return -1;
}

//-----------------------------------------------------------------------------
// FUNCION : int strCnt(char* haystack, char needle[1])
// ACCION : Cuenta cu�ntas de veces que aparece un car�cter dentro de una cadena.
// PARAMETROS: char* haystack -> Lista de caracteres donde se va a contar.
//             char* needle -> Caracter que se debe contar.
// DEVUELVE : La cantidad de veces que aparece el car�cter en la cadena.
//-----------------------------------------------------------------------------
int strCnt(char* haystack, char needle[1])
{
    int c = 0;

    for (int i = 0; haystack[i] != '\0'; i++)
        if (haystack[i] == needle[0])
            c++;

    return c;
}

//-----------------------------------------------------------------------------
// FUNCION : void strInv(char (&strToInv)[200])
// ACCION : Invierte los caracteres de una cadena.
// PARAMETROS: char &strToInv[200] -> Cadena que se desea invertir.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strInv(char (&strToInv)[200])
{
    char strAux[200];

    strCpy(strAux, strToInv);

    int countDown = strLen(strAux);

    for (int i = 0; strAux[i] != '\0'; i++, countDown--)
        strToInv[countDown-1] = strAux[i];
}

//-----------------------------------------------------------------------------
// FUNCION : void strRpl(char (&strToRpl)[200], char chrToRpl[1], int position)
// ACCION : Reemplaza el car�cter de una posici�n dada de una cadena por otro.
// PARAMETROS: char* &strToRpl -> Cadena donde se desea reemplazar.
//             char chrToRpl[1] -> Caracter que se va a reemplazar.
//             int positiion -> Posicion en la que se reemplazar�.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strRpl(char (&strToRpl)[200], char chrToRpl[1], int position)
{
    strToRpl[position] = chrToRpl[0];
}

//-----------------------------------------------------------------------------
// FUNCION : void strTrunc(char (&strToTrunc)[200], int position)
// ACCION : Reemplaza el car�cter de una posici�n dada de una cadena por otro.
// PARAMETROS: char* &strToTrunc -> Cadena que se desea cortar.
//             int positiion -> Posicion en la que se cortar�.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strTrunc(char (&strToTrunc)[200], int position)
{
    strToTrunc[position] = '\0';
}

//-----------------------------------------------------------------------------
// FUNCION : int strSub(char haystack[200], char strToFind[200])
// ACCION : Busca una subcadena dentro de una cadena
// PARAMETROS: char haystack[200] -> Cadena en la que se va a buscar.
//             char needle[200] -> Subcadena en la que se va a buscar.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
int strSub(char haystack[200], char needle[200])
{
    int haystackLenght = strLen(haystack);
    int needleLenght = strLen(needle);

    if (needleLenght > haystackLenght)
        return -1;

    int position = -1;

    for (int i = 0; i < haystackLenght; i++)
        if (haystack[i] == needle[0])
        {
            position = i;
            int aux = i, cont = 0;

            for(int j = 0; j < needleLenght; j++, aux++)
                if (haystack[aux] == needle[j])
                    cont++;

            if (cont == needleLenght)
                return position;

            position = -1;
        }

    return position;
}

//-----------------------------------------------------------------------------
// FUNCION : void strToUpper(char (&strUpper)[200])
// ACCION : Convierte una cadena a mayusculas.
// PARAMETROS: char (&strUpper)[200] -> Cadena a pasar a mayuscula.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strToUpper(char (&strUpper)[200])
{
    for (int i = 0; strUpper[i] != '\0'; i++)
        if ((int) strUpper[i] >= 97 && (int) strUpper[i] <= 122)
            strUpper[i] = ((int) strUpper[i]) - 32;
}

//-----------------------------------------------------------------------------
// FUNCION : void strToLower(char (&strLower)[200])
// ACCION : Convierte una cadena a minusculas.
// PARAMETROS: char (&strLower)[200] -> Cadena a pasar a minuscula.
// DEVUELVE : NADA.
//-----------------------------------------------------------------------------
void strToLower(char (&strLower)[200])
{
    for (int i = 0; strLower[i] != '\0'; i++)
        if ((int) strLower[i] >= 65 && (int) strLower[i] <= 90)
            strLower[i] = ((int) strLower[i]) + 32;
}

//-----------------------------------------------------------------------------
// FUNCION : int strToInt(char strToConvert[200])
// ACCION : Convierte una cadena de caracteres a un n�mero entero.
// PARAMETROS: char strToConvert[200] -> Cadena a convertir en entero.
// DEVUELVE : Numero entero.
//-----------------------------------------------------------------------------
int strToInt(char strToConvert[200])
{
    int n = 0;

    for (int i = 0; strToConvert[i] != '\0'; i++)
    {
        if((int) strToConvert[i] < 48 || (int) strToConvert[i] > 57)
            return -1;

        n = n * 10 + (int) strToConvert[i] - 48;
    }

    return n;
}

//-----------------------------------------------------------------------------
// FUNCION : void intToStr(int number, char (&numberToString)[200])
// ACCION : Convierte un n�mero entero a cadena.
// PARAMETROS: int number -> Numero que se desea pasar a cadena.
//             char (&numberToString)[200] -> Cadena donde se guardar� el numero.
// DEVUELVE : Nada.
//-----------------------------------------------------------------------------
void intToStr(int number, char (&numberToString)[200])
{
    int i, r;

    for (i = 0; number > 0; i++)
    {
        r = number % 10;
        numberToString[i] = (char) r + 48;
        number /= 10;
    }

    numberToString[i] = '\0';
    strInv(numberToString);
}
}
//=============================================================================
//                            FIN DE ARCHIVO
//#############################################################################
